# Privacy Policy for BrainTracker (Android Version)
Last updated: AUG 10TH 2023

Introduction
BrainTracker ("we", "our", or "us") is committed to transparency regarding the collection and use of user data. This Privacy Policy explains how we collect, use, and disclose your information when you use our BrainTracker app ("the App") on the Android platform.

## IMPORTANT NOTICE: All data collected through the App, including personal and conversational data, will be stored in a public repository and will be fully visible to the public. Please do not provide any information that you consider private or sensitive.

Please read this Privacy Policy carefully. If you do not agree with the terms of this Privacy Policy, please do not access or use the App.

### Information We Collect
Personal Information
We may collect personal information such as your name, email address, and other identifiable information that you voluntarily give to us when you register with the App or when you choose to participate in various activities related to the App.

### Conversational Data
We may collect and analyze conversations you have within the App for the purpose of learning about the brain and improving our services. All conversational data will be made publicly available.

### Usage Information
We may collect information about your device and how you use the App, including the operating system, browser type, IP address, and other technical information. This information will also be made publicly available.

### How We Use Your Information
We may use the information we collect from you for various purposes, including to:

### Provide and maintain the App.
Improve, personalize, and expand our services.
Analyze how you use the App.
Make all collected data publicly available in a public repository.
Comply with legal requirements and regulations.
Public Repository
All data collected through the App will be stored in a public repository and will be fully visible and accessible to the public. By using the App, you acknowledge and consent to this public disclosure of your information.

### Your Choices
You may review, change, or terminate your account at any time. Please be aware that any information you have provided will remain publicly available in the public repository.

### Security
While we strive to protect the information we collect, please be aware that no electronic transmission or storage is 100% secure, and we cannot guarantee absolute security, especially as the data is intended to be publicly accessible.

### Children's Privacy
The App is not intended for use by children under the age of 13, and we do not knowingly collect personal information from children under 13.

### Changes to This Privacy Policy
We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page and updating the "last updated" date.

## Contact Us
If you have any questions or concerns about this Privacy Policy, please contact us at:

graylan00@protonmail.com

 
